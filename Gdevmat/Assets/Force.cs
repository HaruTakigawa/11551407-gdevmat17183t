﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Force : MonoBehaviour {
    public float mass;
    public float force;
    public float acceleration;
    public float intForce;
    public Text textForce;
    Animator anim;
    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        GatherForce();
        DisplayForce();
        if (Input.GetKeyDown("space"))
        {
            ApplyForce();
            AttackAnimation();
        }
    }
    void GatherForce()
    {
        force += intForce * Time.deltaTime *100;
        if (force >= 100 || force <= 0)
        {
            intForce *= -1;
        }
    }
    public void DisplayForce()
    {
        textForce.text = "Force: " + Mathf.FloorToInt(force);
    }
    void ApplyForce()
    {
        acceleration = force / mass;
    }
    void AttackAnimation()
    {
        int time = 0;
        if(time >= 5)
        {
            anim.SetBool("Attacking", true);
        }
        else
        {
            anim.SetBool("Attacking", false);
        }
        time++;
    }
}
