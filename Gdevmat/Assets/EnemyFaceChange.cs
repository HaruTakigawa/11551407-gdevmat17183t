﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFaceChange : MonoBehaviour {

    public GameObject player;
    Animator anim;
	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		if(player.GetComponent<Force>().acceleration >= 50)
        {
            anim.SetBool("Hurt", true);
        }
        else
        {
            anim.SetBool("Hurt", false);
        }
	}
}
