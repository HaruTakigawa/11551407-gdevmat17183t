﻿using GDEVMAT_ANYPROJECTNAMEYOUWANT.Utilities;
using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDEVMAT_ANYPROJECTNAMEYOUWANT.Models
{
    public abstract class Movable
    {
        public Vector3 Scale = new Vector3(0.5f, 0.5f, 0.5f);
        public Vector3 Position = new Vector3();
        public Vector3 Velocity = new Vector3();
        public Vector3 Acceleration = new Vector3();
        public float Mass = 1;
        public double red = 1, green = 1, blue = 1, alpha = 1;

        public float G = 0.05f;

        public void ApplyForce(Vector3 force)
        {
            //F =MA
            //A = F/M

            this.Acceleration += (force / Mass);// Force accumulation
        }

        public void ApplyGravity(float scalar = 0.3f)
        {
            this.Acceleration += (new Vector3(0, -scalar*Mass, 0) / Mass);
        }
        public abstract void Render(OpenGL gl);

        public Vector3 CalculateAttraction(Movable movable)
        {
            var force = this.Position - movable.Position;
            var distance = force.GetMagnitude();

            distance = gdevmatUtils.Constrain(distance, 5, 25);

            force.Normalize();

            var strength = (this.G * this.Mass * movable.Mass) / (distance * distance);
            force *= strength;
            return force;
        }
        
    }
}
