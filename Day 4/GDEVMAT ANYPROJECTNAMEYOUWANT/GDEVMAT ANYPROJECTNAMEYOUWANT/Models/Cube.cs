﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDEVMAT_ANYPROJECTNAMEYOUWANT.Models
{
   public class Cube : Movable
    {
     
        public Cube(float x= 0, float y = 0)
        {
            this.posX = x;
            this.posY = y;
        }

        public override void Render(OpenGL gl)
        {
            gl.Color(red, green, blue, alpha);
            gl.Begin(OpenGL.GL_QUADS);
            //front face
            gl.Vertex(this.posX - 0.5f, this.posY + 0.5f);
            gl.Vertex(this.posX + 0.5f, this.posY + 0.5f);
            gl.Vertex(this.posX + 0.5f, this.posY - 0.5f);
            gl.Vertex(this.posX - 0.5f, this.posY - 0.5f);

            gl.End();
        }
    }
}
