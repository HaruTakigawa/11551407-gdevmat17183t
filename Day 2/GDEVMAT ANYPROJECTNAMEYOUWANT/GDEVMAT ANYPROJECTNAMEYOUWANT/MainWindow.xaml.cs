﻿using GDEVMAT_ANYPROJECTNAMEYOUWANT.Models;
using GDEVMAT_ANYPROJECTNAMEYOUWANT.Utilities;
using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;


namespace GDEVMAT_ANYPROJECTNAMEYOUWANT
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //private const int HEADS = 0;
        //private const int TAILS = 1;

        private const int DM = 0;
        private const int N = 1;
        private const int NE = 2;
        private const int E = 3;
        private const int SE = 4;
        private const int S = 5;
        private const int SW = 6;
        private const int W = 7;
        private const int NW = 8;
        private Cube myFirstCube = new Cube();

        private void OpenGLControl_Loaded(object sender, RoutedEventArgs e)
        {

        }
        //float rotation = 0;
        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "GDEVMAT DAY 2";
            OpenGL gl = args.OpenGL;

            // Clear the screen and the depth buffer
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -40.0f);

            myFirstCube.Render(gl);

            gl.DrawText(0, 0, 1, 1, 1, "Arial", 25, RandomGenerator.GenerateInt(0,5) + "");

            /*  int outcome = RandomGenerator.GenerateInt(0,8);
              switch(outcome)
              {
                  case DM:
                      gl.Color(1.0f, 1.0f, 1.0f);
                      break;

                  case N:
                      myFirstCube.posY++;
                      gl.Color(0.0f, 1.0f, 1.0f);
                      break;
                  case NE:
                      myFirstCube.posY++;
                      myFirstCube.posX++;
                      gl.Color(1.0f, 0.0f, 1.0f);
                      break;
                  case E:
                      myFirstCube.posX++;
                      gl.Color(1.0f, 1.0f, 0.0f);
                      break;
                  case SE:
                      myFirstCube.posY--;
                      myFirstCube.posX++;
                      gl.Color(0.0f, 0.0f, 1.0f);
                      break;
                  case S:
                      myFirstCube.posY--;
                      gl.Color(1.0f, 0.0f, 0.0f);
                      break;
                  case SW:
                      myFirstCube.posY--;
                      myFirstCube.posX--;
                      gl.Color(0.0f, 1.0f, 0.0f);
                      break;
                  case W:
                      myFirstCube.posX--;
                      gl.Color(0.87f, 0.81f, 0.98f);
                      break;
                  case NW:
                      myFirstCube.posY++;
                      myFirstCube.posX--;
                      gl.Color(1.0f, 0.5f, 1.0f);
                      break;

              }
              */

            myFirstCube.color = new Models.Color(RandomGenerator.GenerateDouble(0, 1), RandomGenerator.GenerateDouble(0, 1), RandomGenerator.GenerateDouble(0, 1));
            double outcomeWithDiffProbability = RandomGenerator.GenerateDouble(0,1);
            if (outcomeWithDiffProbability < 0.2)
            {
                myFirstCube.posY++;
            }
            else if (outcomeWithDiffProbability >= 0.2 && outcomeWithDiffProbability < 0.4)
            {
                myFirstCube.posY--;
            }
            else if (outcomeWithDiffProbability >= 0.4 && outcomeWithDiffProbability < 0.6)
            {
                myFirstCube.posX--;
            }
            else
            {
                myFirstCube.posX++;
            }

        }

        #region INITIALIZATION

        public MainWindow()
        {
            InitializeComponent();
        }

        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;
            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }
#endregion
    }
}
