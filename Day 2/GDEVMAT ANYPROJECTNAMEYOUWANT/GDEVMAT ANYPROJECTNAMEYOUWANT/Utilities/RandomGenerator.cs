﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDEVMAT_ANYPROJECTNAMEYOUWANT.Utilities
{
    class RandomGenerator
    {
        private static readonly Random random = new Random();

        public static int GenerateInt(int min = 0, int max = 1)
        {
            return random.Next(min, max + 1);
        }
        public static double GenerateDouble(double min = 0, int max = 1)
        {
            return random.NextDouble() * (max - min) + min;
        }
    }
}
