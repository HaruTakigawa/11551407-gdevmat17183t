﻿using SharpGL;
using SharpGL.SceneGraph.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GDEVMAT_ANYPROJECTNAMEYOUWANT
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const float LINE_SMOOTHNESS = 0.02f;
        private const float GRAPH_LIMIT = 15;
        private const float TOTAL_CIRCLE_ANGLE = 360;
        public MainWindow()
        {
            InitializeComponent();
        }
        private float frequency = 2;
        private float amplitude = 1;
        private int t = 0;
        private void OpenGLControl_Loaded(object sender, RoutedEventArgs e)
        {

        }
        //float rotation = 0;
        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "GDEVMAT DAY 1";
            OpenGL gl = args.OpenGL;

            // Clear the screen and the depth buffer
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -40.0f);

            /*gl.Rotate(rotation, 0.0f, 1.0f, 0.0f);
            Teapot tp = new Teapot();
            tp.Draw(gl, 14, 1, OpenGL.GL_FILL);

            rotation += 3.0f;
            */

            // Draw point
            gl.PointSize(10.0f);
            gl.Begin(OpenGL.GL_POINTS);
            gl.Vertex(0, 0);
            gl.End();

            //// Draw line
            //gl.LineWidth(2.0f);
            //gl.Begin(OpenGL.GL_LINE_STRIP);
            //gl.Vertex(-5, 5);
            //gl.Vertex(6, -3);
            //gl.End();

            // y axis
            gl.LineWidth(2.0f);
            gl.Begin(OpenGL.GL_LINE_STRIP);
            gl.Vertex(0, 15);
            gl.Vertex(0, -15);
            gl.End();

            // x axis
            gl.LineWidth(2.0f);
            gl.Begin(OpenGL.GL_LINE_STRIP);
            gl.Vertex(15, 0);
            gl.Vertex(-15, 0);
            gl.End();

            for (int i = -15; i <= 15; i++)
            {
                gl.LineWidth(1.0f);
                gl.Begin(OpenGL.GL_LINE_STRIP);
                gl.Vertex(.5, i);
                gl.Vertex(-.5, i);
                gl.End();

                gl.LineWidth(1.0f);
                gl.Begin(OpenGL.GL_LINE_STRIP);
                gl.Vertex(i, .5);
                gl.Vertex(i, -.5);
                gl.End();
            }

            //Linear Functions
            gl.PointSize(1.0f);
            gl.Begin(OpenGL.GL_POINTS);
            for (float x = -(GRAPH_LIMIT - 5); x <= (GRAPH_LIMIT - 5); x += LINE_SMOOTHNESS)
            {
                gl.Vertex(x, (2 * x) + 3);
            }
            gl.End();

            //Quadratic Function
            gl.PointSize(1.0f);
            gl.Begin(OpenGL.GL_POINTS);
            for (float x = -(GRAPH_LIMIT - 5); x <= (GRAPH_LIMIT - 5); x += LINE_SMOOTHNESS)
            {
                gl.Vertex(x, -(x * x) + (3 * x) - 4);
            }
            gl.End();

            //Circle
            float radius = 5.0f;
            gl.PointSize(1.0f);
            gl.Begin(OpenGL.GL_POINTS);
            for(int i = 0; i<= TOTAL_CIRCLE_ANGLE;i++)
            {
                gl.Vertex(Math.Cos(i) * radius, Math.Sin(i) * radius);
            }
            gl.End();

            // Sin Wave

            gl.Begin(OpenGL.GL_POINTS);
            for (double x = -10; x <= 10; x += LINE_SMOOTHNESS)
            {
                gl.Vertex(x, (amplitude * Math.Sin(x + t* frequency)));
            }
            gl.End();

            t += 1;
            if (t == 360) t = 0;
            if (Keyboard.IsKeyDown(Key.Down)) amplitude++;
            if (Keyboard.IsKeyDown(Key.Up)) amplitude--;
            if (Keyboard.IsKeyDown(Key.Left)) frequency--;
            if (Keyboard.IsKeyDown(Key.Right)) frequency++;
        }

        #region OPENGL INITIALIZATION
        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;
            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }
#endregion
    }
}
