﻿using GDEVMAT_ANYPROJECTNAMEYOUWANT.Models;
using GDEVMAT_ANYPROJECTNAMEYOUWANT.Utilities;
using SharpGL;
using System.Collections.Generic;
using System.Windows;


namespace GDEVMAT_ANYPROJECTNAMEYOUWANT
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private Cube myCube = new Cube();
        private Circle myCircle = new Circle();
        private Vector3 mousePosition = new Vector3();
        private Vector3 gravity = new Vector3(0, -0.5f, 0);
        private Vector3 wind = new Vector3(0.05f, 0, 0);
        private int time;
        private List<Circle> circles = new List<Circle>();
        private List<Cube> cubes = new List<Cube>();
        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "GDEVMAT DAY 9";
            OpenGL gl = args.OpenGL;

            // Clear the screen and the depth buffer
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -60.0f);
           
          
            
            foreach (var c in cubes)
            {
                c.Render(gl);
                //c.ApplyForce(gravity);
                c.ApplyGravity();
                c.ApplyForce(wind);

                //friction = -uNV
                var coefficient = 0.01f;
                var normal = 1;
                var frictionMagnitude = coefficient * normal;
                var friction = c.Velocity;
                friction *= -1;
                friction.Normalize();
                friction *= frictionMagnitude;
                c.ApplyForce(friction);
                c.Scale = new Vector3(c.Mass / 2, c.Mass/ 2, c.Mass / 2);
                if (c.Position.y <= -25 || c.Position.y >= 25)
                {
                    c.Position.y = -25;
                    c.Velocity.y *= -1f;
                }
                if(c.Position.x <= -40 || c.Position.x >= 40)
                {
                    c.Velocity.x *= -1f;
                }
                
            }
            time++;
            if (time == 300)
            {
                circles.Clear();
                cubes.Clear();
                time = 0;
            }



        }
        #region INITIALIZATION

        public MainWindow()
        {
            InitializeComponent();
            for(int i = 0; i <= 10; i++)
            {
                cubes.Add(new Cube()
                {
                    red = (double)RandomGenerator.GenerateGaussian(0, 15),
                    blue = (double)RandomGenerator.GenerateGaussian(0, 15),
                    green = (double)RandomGenerator.GenerateGaussian(0, 15),
                    Position = new Vector3(-10, 20, 0),
                    Mass = cubes.Count + 1
                });
            }
          
         
        }

        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;
            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);

            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_CONSTANT_ALPHA_EXT);
            gl.Enable(OpenGL.GL_BLEND);
            gl.ClearColor(0, 0, 0, 0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }
        private void OpenGLControl_Loaded(object sender, RoutedEventArgs e)
        {

        }
        #endregion

        private void OpenGLControl_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            var position = e.GetPosition(this);
            mousePosition.x = (float)position.X - (float)Width / 2.0f;
            mousePosition.y = -((float)position.Y - (float)Height / 2.0f);
        }
    }
}
